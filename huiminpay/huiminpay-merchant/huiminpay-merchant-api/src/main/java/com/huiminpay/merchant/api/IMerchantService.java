package com.huiminpay.merchant.api;


import com.huiminpay.common.cache.domain.BusinessException;
import com.huiminpay.merchant.api.dto.MerchantDTO;
import com.huiminpay.merchant.api.dto.StaffDTO;
import com.huiminpay.merchant.api.dto.StoreDTO;

public interface IMerchantService {
    /**
     * 根据ID查询商户信息
     *
     * @return
     */

    public MerchantDTO queryMerchantById(Long id);

    /**
     * 注册商户服务
     * @param merchantDTO
     * @return
     * @throws BusinessException
     */
    MerchantDTO reg(MerchantDTO merchantDTO) throws BusinessException;

    /**
     * 更新商户信息
     * @param merchantDTO
     * @return
     * @throws BusinessException
     */
    MerchantDTO applay(MerchantDTO merchantDTO)throws BusinessException;

    /**
     * 新增门店
     * @param storeDTO 门店信息
     * @return 新增成功的门店信息
     * @throws BusinessException
     */
    StoreDTO createStore(StoreDTO storeDTO) throws BusinessException;

    /**
     * 新增员工
     *
     * @param staffDTO 员工信息
     * @return 返回新增成功的员工信息
     * @throws BusinessException
     */
    StaffDTO createStaff(StaffDTO staffDTO) throws BusinessException;

    /**
     * 将员工设置为门店管理员
     * @param storeId 门店ID
     * @param staffId 员工ID
     * @throws BusinessException
     */
    void bindStaffToStore(Long storeId, Long staffId) throws BusinessException;

    MerchantDTO queryMerchantByTenantId(Long tenantId);
}
