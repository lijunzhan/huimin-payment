package com.huiminpay.merchant.api.service;

import com.huiminpay.common.cache.domain.BusinessException;
import com.huiminpay.merchant.api.dto.AppDTO;

import java.util.List;

public interface IAppservice {
    /**
     * 商户应用创建
     * @param appDTO
     * @return
     */
    AppDTO createApp(AppDTO appDTO) throws BusinessException;

    /**
     * 根据业务Id查询应用详情
     * @param appId
     * @return
     */
    AppDTO myapp(String appId);

    /**
     * 查询商户名下的应用
     * @param merchantId
     * @return
     */
    List<AppDTO> myApps(Long merchantId);
}
