package com.huiminpay.merchant.api.service;

import com.huiminpay.common.cache.domain.BusinessException;
import com.huiminpay.merchant.api.dto.MerchantDTO;
import com.huiminpay.merchant.api.dto.StaffDTO;

public interface IStaffservice {
    /**
     * 商户下新增员工
     * @param staffDTO
     * @param
     * @return
     * @throws BusinessException
     */
    StaffDTO addStafftoMerchant(StaffDTO staffDTO)throws BusinessException;

    /**
     * 编辑商户下所属员工
     * @param staffDTO
     * @return
     * @throws BusinessException
     */

    StaffDTO updataStafftoMerchant(StaffDTO staffDTO)throws BusinessException;

    /**
     * 删除商户下所属员工
     * @param
     * @return
     * @throws BusinessException
     */

    void deleteStafftoMerchant(Long id)throws BusinessException;


}
