package com.huiminpay.merchant.api.service;

import com.huiminpay.common.cache.domain.PageVO;
import com.huiminpay.merchant.api.dto.StoreDTO;
import com.huiminpay.merchant.api.dto.QueryStoreDto;
public interface IStoreService {
    /**
     * 门店列表查询以及分页
     * @param queryStoreDto 参数
     * @return 分页数据
     */
    PageVO<StoreDTO> queryStoreByPage(QueryStoreDto queryStoreDto);

    StoreDTO selectById(Long storeId);
}
