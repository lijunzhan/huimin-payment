package com.huiminpay.merchant.convert;

import com.huiminpay.merchant.api.dto.MerchantDTO;
import com.huiminpay.merchant.entity.Merchant;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface MerchantConverMapper {
    MerchantConverMapper INSTANCE = Mappers.getMapper(MerchantConverMapper.class);

    MerchantDTO entity2dto(Merchant merchant);

    Merchant dto2entity(MerchantDTO merchant);
}
