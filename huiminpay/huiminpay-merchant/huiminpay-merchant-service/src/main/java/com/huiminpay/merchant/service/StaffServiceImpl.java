package com.huiminpay.merchant.service;

import com.huiminpay.common.cache.domain.BusinessException;
import com.huiminpay.merchant.api.dto.MerchantDTO;
import com.huiminpay.merchant.api.dto.StaffDTO;
import com.huiminpay.merchant.api.service.IStaffservice;
import com.huiminpay.merchant.convert.StaffConvert;
import com.huiminpay.merchant.entity.Staff;
import com.huiminpay.merchant.mapper.MerchantMapper;
import com.huiminpay.merchant.mapper.StaffMapper;
import com.huiminpay.merchant.mapper.StoreMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
@Slf4j
public class StaffServiceImpl implements IStaffservice {
    @Autowired
    MerchantMapper merchantMapper;

    @Autowired
    StoreMapper storeMapper;

    @Autowired
    StaffMapper staffMapper;

    /**
     * 商户下新增员工
     *
     * @param staffDTO
     * @param
     * @return
     */
    @Override
    public StaffDTO addStafftoMerchant(StaffDTO staffDTO) throws BusinessException {

        Staff staff = StaffConvert.INSTANCE.dto2entity(staffDTO);
        log.info("商户下新增员工");
        staffMapper.insert(staff);
        return StaffConvert.INSTANCE.entity2dto(staff);
    }

    /**
     * 编辑商户下所属员工
     *
     * @param staffDTO
     * @return
     * @throws BusinessException
     */
    @Override
    public StaffDTO updataStafftoMerchant(StaffDTO staffDTO) throws BusinessException {

        Staff staff = StaffConvert.INSTANCE.dto2entity(staffDTO);
        log.info("编辑商户下员工");
        staffMapper.updateById(staff);
        return StaffConvert.INSTANCE.entity2dto(staff);
    }

    /**
     * 删除商户下所属员工
     *
     * @param
     * @return
     * @throws BusinessException
     */
    @Override
    public void deleteStafftoMerchant(Long id) throws BusinessException {

        staffMapper.deleteById(id);
    }


}
