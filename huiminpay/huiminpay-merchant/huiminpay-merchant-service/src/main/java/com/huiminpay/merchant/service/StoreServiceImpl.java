package com.huiminpay.merchant.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huiminpay.common.cache.domain.PageVO;
import com.huiminpay.merchant.api.dto.QueryStoreDto;
import com.huiminpay.merchant.api.dto.StoreDTO;
import com.huiminpay.merchant.api.service.IStoreService;
import com.huiminpay.merchant.convert.StoreConvert;
import com.huiminpay.merchant.entity.Store;
import com.huiminpay.merchant.mapper.StoreMapper;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class StoreServiceImpl implements IStoreService {
    @Autowired
    StoreMapper storeMapper;


    /**
     * 门店列表信息查询以及分页
     *
     * @param queryStoreDto c参数
     * @return 分页数据
     */
    @Override
    public PageVO<StoreDTO> queryStoreByPage(QueryStoreDto queryStoreDto) {

        Page<Store> page = new Page<>(queryStoreDto.getPageNo(),queryStoreDto.getPageSize());

        QueryWrapper<Store> qw = new QueryWrapper<>();//查询构造条件的构造器;
        qw.lambda().eq(Store::getMerchantId,queryStoreDto.getMerchantId());
        IPage<Store> storeIPage = storeMapper.selectPage(page, qw);

        List<StoreDTO> storeDTOS = StoreConvert.INSTANCE.listentity2listdto(storeIPage.getRecords());

        PageVO<StoreDTO> pageVO = new PageVO<>();
        pageVO.setCounts(storeIPage.getTotal());
        pageVO.setPage(queryStoreDto.getPageNo());
        pageVO.setPageSize(queryStoreDto.getPageSize());
        pageVO.setItems(storeDTOS); //分页数据
        return pageVO;
    }

    @Override
    public StoreDTO selectById(Long storeId) {
        Store store = storeMapper.selectById(storeId);
        StoreDTO storeDTO = new StoreDTO();
        BeanUtils.copyProperties(store,storeDTO);
        return storeDTO;
    }
}
