package com.huiminpay.merchant.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.huiminpay.common.cache.domain.BusinessException;
import com.huiminpay.common.cache.domain.CommonErrorCode;
import com.huiminpay.common.cache.util.RandomUuidUtil;
import com.huiminpay.merchant.api.dto.AppDTO;
import com.huiminpay.merchant.api.service.IAppservice;
import com.huiminpay.merchant.convert.AppConvert;
import com.huiminpay.merchant.entity.App;
import com.huiminpay.merchant.mapper.AppMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;


@Service
public class AppServiceImpl implements IAppservice {
    @Autowired
    AppMapper appMapper;

    /**
     * 商户创建应用
     * @param appDTO
     * @return
     */
    @Override
    public AppDTO createApp(AppDTO appDTO)throws BusinessException {
        //1.非空校验
        if (StringUtils.isEmpty(appDTO.getAppName())){
            throw new BusinessException(CommonErrorCode.E_200001);
        }

        if (appDTO.getMerchantId() == null){
            throw new BusinessException(CommonErrorCode.E_200002);
        }
        //2.对象转换
        App app = AppConvert.INSTANCE.dto2entity(appDTO);

        //3.保存数据库  APPId需要自己生成 UUID
        app.setAppId(RandomUuidUtil.getUUID());
        appMapper.insert(app);

        return AppConvert.INSTANCE.entity2dto(app);
    }

    @Override
    public AppDTO myapp(String appId) {
        QueryWrapper<App> qw = new QueryWrapper<>();
        qw.lambda().eq(App::getAppId,appId);
        App app = appMapper.selectOne(qw);
        return AppConvert.INSTANCE.entity2dto(app);
    }


    //查询商户下的应用
    @Override
    public List<AppDTO> myApps(Long merchantId) {
        QueryWrapper<App> qw = new QueryWrapper<>();
        qw.lambda().eq(App::getMerchantId,merchantId);
        List<App> apps = appMapper.selectList(qw);


        return AppConvert.INSTANCE.listentity2listdto(apps);
    }
}
