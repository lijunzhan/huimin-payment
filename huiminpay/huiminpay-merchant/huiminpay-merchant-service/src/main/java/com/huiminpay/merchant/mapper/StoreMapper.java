package com.huiminpay.merchant.mapper;

import com.huiminpay.merchant.entity.Store;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lijunzhan
 * @since 2021-07-20
 */
@Repository
public interface StoreMapper extends BaseMapper<Store> {

}
