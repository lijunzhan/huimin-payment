package com.huiminpay.merchant.service;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.huiminpay.common.cache.domain.BusinessException;
import com.huiminpay.common.cache.domain.CommonErrorCode;
import com.huiminpay.merchant.api.IMerchantService;
import com.huiminpay.merchant.api.dto.MerchantDTO;
import com.huiminpay.merchant.api.dto.StaffDTO;
import com.huiminpay.merchant.api.dto.StoreDTO;
import com.huiminpay.merchant.convert.MerchantConverMapper;
import com.huiminpay.merchant.convert.StaffConvert;
import com.huiminpay.merchant.convert.StoreConvert;
import com.huiminpay.merchant.entity.Merchant;
import com.huiminpay.merchant.entity.Staff;
import com.huiminpay.merchant.entity.Store;
import com.huiminpay.merchant.entity.StoreStaff;
import com.huiminpay.merchant.mapper.MerchantMapper;
import com.huiminpay.merchant.mapper.StaffMapper;
import com.huiminpay.merchant.mapper.StoreMapper;
import com.huiminpay.merchant.mapper.StoreStaffMapper;
import com.huiminpay.user.api.TenantService;
import com.huiminpay.user.api.dto.tenant.CreateTenantRequestDTO;
import com.huiminpay.user.api.dto.tenant.TenantDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
@Service
public class MerchantServiceImpl implements IMerchantService {

    @Autowired
    MerchantMapper merchantMapper;

    @Autowired
    StoreMapper storeMapper;

    @Autowired
    StaffMapper staffMapper;

    @Autowired
    StoreStaffMapper storeStaffMapper;

    @Reference
    TenantService tenantService;

    //根据Id查询商户
    @Override
    public MerchantDTO queryMerchantById(Long id) {
        Merchant merchant = merchantMapper.selectById(id);
//        MerchantDTO merchantDTO = new MerchantDTO();
////        merchantDTO.setId(merchant.getId());
//        BeanUtils.copyProperties(merchant,merchantDTO);
        //设置其他属性...
        return MerchantConverMapper.INSTANCE.entity2dto(merchant);
    }

    //添加商户

    /**
     * 注册商户服务接口 接收账号 密码 手机号 为了可扩展性使用merchantDTO接收数据
     * 调用SaaS接口：完成新增租户、用户、绑定租户和用户的关系、初始化权限
     * @param merchantDTO
     * @return
     * @throws BusinessException
     */
    @Override
    public MerchantDTO reg(MerchantDTO merchantDTO)throws BusinessException{
        //1.非空校验
        if (StringUtils.isEmpty(merchantDTO.getMobile())){
            throw new BusinessException(CommonErrorCode.E_200230);
        }

        if (StringUtils.isEmpty(merchantDTO.getUsername())){
            throw new BusinessException(CommonErrorCode.E_200231);
        }

        if (StringUtils.isEmpty(merchantDTO.getPassword())){
            throw new BusinessException(CommonErrorCode.E_200232);
        }
        //2.手机号唯一校验
        QueryWrapper<Merchant> qw = new QueryWrapper<>();
        qw.lambda().eq(Merchant::getMobile,merchantDTO.getMobile());
        Integer integer = merchantMapper.selectCount(qw);
        if (integer > 0){
            throw new BusinessException(CommonErrorCode.E_200203);
        }

        //手机号不存在时调用saas接口
          //1.构建调用参数
        /**
         * 接口参数：
         * 1、手机号
         * 2、账号
         * 3、密码
         * 4、租户类型：huimin-merchant
         * 5、默认套餐：huimin-merchant
         * 6、租户名称，同账号名
         */
        CreateTenantRequestDTO createTenantRequestDTO = new CreateTenantRequestDTO();
        createTenantRequestDTO.setMobile(merchantDTO.getMobile());
        createTenantRequestDTO.setUsername(merchantDTO.getUsername());
        createTenantRequestDTO.setPassword(merchantDTO.getPassword());
        createTenantRequestDTO.setTenantTypeCode("huimin-merchant"); //租户类型
        createTenantRequestDTO.setBundleCode("huimin-merchant");// 套餐名称  根据套餐下的权限给用户分配权限
        createTenantRequestDTO.setName(merchantDTO.getUsername()); //租户名称   和账号保持一致

        //如果租户在saas已经存在，SaaS直接返回此租户的信息，如果没有则进行添加
        TenantDTO tenantAndAccount = tenantService.createTenantAndAccount(createTenantRequestDTO);
        //获取租户Id
        if (tenantAndAccount == null || tenantAndAccount.getId() == null){
            throw new BusinessException(CommonErrorCode.E_200237);
        }
        //租户id
        Long tenantId = tenantAndAccount.getId();

        //租户id在商户表唯一
        //根据租户id在商户表查询，如果存在记录则不允许添加记录
        Integer count = merchantMapper.selectCount(new LambdaQueryWrapper<Merchant>().eq(Merchant::getTenantId, tenantId));
        if (count>0){
            throw new BusinessException(CommonErrorCode.E_200017);
        }

        //3.保存数据库
        Merchant merchant = MerchantConverMapper.INSTANCE.dto2entity(merchantDTO);
        //设置对应的租户id
        merchant.setTenantId(tenantId);

        //业务状态更新 例如商户状态 审核状态 0-未申请,1-已申请待审核,2-审核通过,3-审核拒绝
        merchant.setAuditStatus("0");

        //新增门店
        StoreDTO storeDTO = new StoreDTO();
        storeDTO.setStoreName("根门店");
        storeDTO.setMerchantId(merchant.getId()); //商户Id
//        storeDTO.setStoreStatus(true);  //门店状态 true：已启用
        StoreDTO store = createStore(storeDTO);

        //新增员工
        StaffDTO staffDTO = new StaffDTO();
        staffDTO.setMobile(merchantDTO.getMobile());  //员工手机号
        staffDTO.setUsername(merchantDTO.getUsername()); //员工账号
        staffDTO.setStoreId(store.getId());  //员工所属门店id
        staffDTO.setMerchantId(merchant.getId()); //商户id
        StaffDTO staff = createStaff(staffDTO);

        //为门店设置管理员
         //将门店和员工的id传入  设置门店和员工的关系
        bindStaffToStore(store.getId(),staff.getId());

        //调用mapper向数据库写入记录
        merchantMapper.insert(merchant);
//        merchantDTO.setId(merchant.getId());
        return MerchantConverMapper.INSTANCE.entity2dto(merchant);
    }

    //更新商户信息
    @Override
    public MerchantDTO applay(MerchantDTO merchantDTO) throws BusinessException {
        Merchant merchant = MerchantConverMapper.INSTANCE.dto2entity(merchantDTO);
        //设置不更新属性
        merchant.setMobile(null);
        merchant.setUsername(null);
        merchant.setTenantId(null);

        merchant.setAuditStatus("1");
        merchantMapper.updateById(merchant);
        return MerchantConverMapper.INSTANCE.entity2dto(merchant);
    }

    /**
     *新增门店
     * @param storeDTO 门店信息
     * @return
     * @throws BusinessException
     */
    @Override
    public StoreDTO createStore(StoreDTO storeDTO) throws BusinessException {
        Store store = StoreConvert.INSTANCE.dto2entity(storeDTO);
        log.info("新增门店:{}", JSON.toJSONString(store));
        //新增门店
        storeMapper.insert(store);
        //返回门店信息
        return StoreConvert.INSTANCE.entity2dto(store);
    }

    /**
     * 新增员工
     *
     * @param staffDTO 员工信息
     * @return 返回新增成功的员工信息
     * @throws BusinessException
     */
    @Override
    public StaffDTO createStaff(StaffDTO staffDTO) throws BusinessException {
        //参数合法性校验
        if (staffDTO == null || StringUtils.isBlank(staffDTO.getMobile())
                             || StringUtils.isBlank(staffDTO.getUsername())
                             || staffDTO.getStoreId() == null){
            throw new BusinessException(CommonErrorCode.E_200201);
        }
        //同一个商户下员工的账号唯一
        Boolean existStaffByUserName = isExistStaffByUserName(staffDTO.getUsername(), staffDTO.getMerchantId());
        if (existStaffByUserName){
            throw new BusinessException(CommonErrorCode.E_200225);
        }
        //同一个商户下员工的手机号唯一
        Boolean existStaffByMobile = isExistStaffByMobile(staffDTO.getMobile(), staffDTO.getMerchantId());
        if (existStaffByMobile){
            throw new BusinessException(CommonErrorCode.E_200203);
        }
        //将数据保存到数据库
        Staff staff = StaffConvert.INSTANCE.dto2entity(staffDTO);
        log.info("商户下新增员工");
        staffMapper.insert(staff);
        return StaffConvert.INSTANCE.entity2dto(staff);
    }

    /**
     * 将员工设置为门店管理员
     *
     * @param storeId 门店ID
     * @param staffId 员工ID
     * @throws BusinessException
     */
    @Override
    public void bindStaffToStore(Long storeId, Long staffId) throws BusinessException {
        StoreStaff storeStaff = new StoreStaff();
        storeStaff.setStoreId(storeId);//门店Id
        storeStaff.setStaffId(staffId);//员工Id
        storeStaffMapper.insert(storeStaff);
    }

    @Override
    public MerchantDTO queryMerchantByTenantId(Long tenantId) {
        QueryWrapper<Merchant> qw = new QueryWrapper<>();
        qw.lambda().eq(Merchant::getTenantId,tenantId);
        Merchant merchant = merchantMapper.selectOne(qw);
        return MerchantConverMapper.INSTANCE.entity2dto(merchant);
    }

    /**
     * 在同一商户下员工的账号唯一校验
     * @param username
     * @param merchantId
     * @return
     */
    Boolean isExistStaffByUserName(String username,Long merchantId){
        Integer integer = staffMapper.selectCount(new LambdaQueryWrapper<Staff>().eq(Staff::getUsername,username)
                .eq(Staff::getMerchantId, merchantId));
        return integer>0;
    }

    /**
     * 员工手机号在同一商户下的唯一校验
     * @param mobile
     * @param merchantId
     * @return
     */
    Boolean isExistStaffByMobile(String mobile,Long merchantId){
        Integer integer = staffMapper.selectCount(new LambdaQueryWrapper<Staff>().eq(Staff::getMobile, mobile)
                .eq(Staff::getMerchantId, merchantId));
        return integer>0;
    }
}
