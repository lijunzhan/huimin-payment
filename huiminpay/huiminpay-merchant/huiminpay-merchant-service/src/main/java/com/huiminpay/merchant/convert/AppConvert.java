package com.huiminpay.merchant.convert;

import com.huiminpay.merchant.api.dto.AppDTO;
import com.huiminpay.merchant.entity.App;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

@Mapper
//格式转换器
public interface AppConvert {
    AppConvert INSTANCE = Mappers.getMapper(AppConvert.class); //相当于new了一个实现类

    AppDTO entity2dto(App app);

    App dto2entity(AppDTO appDTO);


    List<AppDTO> listentity2listdto(List<App> apps);

}
