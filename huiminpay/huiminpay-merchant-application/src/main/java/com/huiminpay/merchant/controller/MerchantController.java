package com.huiminpay.merchant.controller;


import com.aliyun.oss.OSS;
import com.aliyun.oss.model.PutObjectRequest;
import com.huiminpay.common.cache.domain.BusinessException;
import com.huiminpay.common.cache.domain.CommonErrorCode;
import com.huiminpay.common.cache.domain.PageVO;
import com.huiminpay.common.cache.util.PhoneUtil;
import com.huiminpay.common.cache.util.QRCodeUtil;
import com.huiminpay.merchant.api.IMerchantService;
import com.huiminpay.merchant.api.dto.MerchantDTO;

import com.huiminpay.merchant.api.dto.QueryStoreDto;
import com.huiminpay.merchant.api.dto.StoreDTO;
import com.huiminpay.merchant.api.service.IStoreService;
import com.huiminpay.merchant.common.SecurityUtil;
import com.huiminpay.merchant.convert.MerchantConvert;
import com.huiminpay.merchant.service.SmsService;
import com.huiminpay.merchant.vo.MerchantVo;
import com.huiminpay.merchant.vo.RegMchVo;
import com.huiminpay.transaction.api.dto.QrcodeDTO;
import com.huiminpay.transaction.api.service.ITransactionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


@RestController
@Api(value = "商户平台应用接口",tags = "商户平台应用接口",description = "1+1=3")
public class MerchantController {
    @Reference  //接口生成代理对象
    IMerchantService merchantService;

    @Autowired
    SmsService smsService;
    @Autowired
    RestTemplate restTemplate;
    @Autowired
    OSS oss;
    @Reference
    IStoreService storeService;
    @Reference
    ITransactionService transactionService;

    @Value("${aliyun.oss.domain}")
    private String domain;

    @Value("${aliyun.oss.bucketName}")
    private String bucketName;

    //远程调用
    @GetMapping("/merchants/{id}")
    @ApiOperation("根据Id查询商户信息")
    @ApiImplicitParam(value = "商户Id" ,name = "id",example = "1",dataType = "Long",paramType = "path")
    public Object queryMerchantById(@PathVariable("id") Long id){
        MerchantDTO merchantDTO = merchantService.queryMerchantById(id);
        return MerchantConvert.INSTANCE.dto2vo(merchantDTO);
    }

    @ApiOperation("获取登录用户的商户信息")
    @GetMapping(value="/my/merchants")
    public MerchantDTO getMyMerchantInfo(){
        // 从token 中获取商户id
        Long merchantId = SecurityUtil.getMerchantId();
        MerchantDTO merchant = merchantService.queryMerchantById(merchantId);
        return merchant;
    }

    @ApiOperation("商户注册")
    @PostMapping("/merchants/register")
//    @ApiImplicitParam(value = "手机号" ,name = "phone",required = true,dataType = "string",paramType = "query")
    public Object register(@RequestBody RegMchVo regMchVo){
        //1.非空校验
        if (StringUtils.isEmpty(regMchVo.getVerifiyCode())){
//            throw new RuntimeException("验证码不能为空");
            throw new BusinessException(CommonErrorCode.E_100103);
        }

        if (StringUtils.isEmpty(regMchVo.getMobile())){
            //throw new RuntimeException("手机号不能为空");
            throw new BusinessException(CommonErrorCode.E_200230);

        }

        if (StringUtils.isEmpty(regMchVo.getUsername())){
            throw new RuntimeException("用户名不能为空");
        }

        if (StringUtils.isEmpty(regMchVo.getPassword())){
            throw new RuntimeException("密码不能为空");
        }
        //2.验证码校验

        if (!PhoneUtil.isMatches(regMchVo.getMobile())){
//            throw new RuntimeException("手机号格式不正确");
            throw new BusinessException(CommonErrorCode.E_200204);
        }

        Boolean aBoolean = smsService.verifyCode(regMchVo.getVerifiyCode(), regMchVo.getVerifiykey());
        if (!aBoolean){
//            throw new RuntimeException("验证码过期或错误");
            throw new BusinessException(CommonErrorCode.E_100102);
        }
        //3.保存数据
//        MerchantDTO merchantDTO = new MerchantDTO();
//        BeanUtils.copyProperties(regMchVo,merchantDTO);
        MerchantDTO merchantDTO = MerchantConvert.INSTANCE.vo2dto(regMchVo);
        return merchantService.reg(merchantDTO);
    }

    @ApiOperation("文件上传")
    @PostMapping("/upload")
    public Object upload(@RequestParam("file") MultipartFile file){
        //1.非空判断
        if (file == null){
            throw new BusinessException(CommonErrorCode.E_200201);
        }

        //时间戳作为当前上传文件名 保证文件名不重复
        long name = System.currentTimeMillis();

        //拿到扩展名
        String originalFilename = file.getOriginalFilename();//源文件名
        //通过源文件名获取扩展名
        String extType = originalFilename.substring(originalFilename.lastIndexOf("."));

        //拼接文件名+扩展名
        String fileName = name + extType;
        try {
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, fileName,file.getInputStream());

            //上传字符串
            oss.putObject(putObjectRequest);
        } catch (IOException e) {
            e.printStackTrace();
            throw new BusinessException(CommonErrorCode.E_100106);

        }
        return domain+fileName;
    }

    @ApiOperation("商户资质申请")
    @PostMapping("/my/merchants/save")
    public Object applay(@RequestBody MerchantVo merchantVo){
        //1.非空校验
        if (merchantVo.getId() == null){
            throw new BusinessException(CommonErrorCode.E_200201);
        }
        //2.对象转换
        MerchantDTO merchantDTO = MerchantConvert.INSTANCE.vo2dto(merchantVo);
        //3.更新数据
        Long merchantId = SecurityUtil.getMerchantId(); //模拟登录
        merchantDTO.setId(merchantId);
        merchantDTO = merchantService.applay(merchantDTO);
        return merchantDTO;
    }

    @ApiOperation("短信发送")
    @GetMapping("sms")
    @ApiImplicitParam(value = "手机号" ,name = "mobile",example = "195****3545",dataType = "string",paramType = "query")
    public Object sendSms(String mobile){
       return smsService.sendSms(mobile);
    }

    @ApiOperation("测试2")
    @ApiImplicitParam(name = "name", value = "姓名", required = true, dataType = "string")
    @PostMapping(value = "/hi")
    public String hi(String name) {
        return "hi," + name;
    }




    @ApiOperation("商户下的门店管理")
    @PostMapping("/my/stores/merchants/page")
    public PageVO<StoreDTO> queryStoreByPage(@RequestParam Integer pageNo, @RequestParam Integer
            pageSize){

        Long merchantId = SecurityUtil.getMerchantId();// 获取商户ID
        QueryStoreDto queryStoreDto = new QueryStoreDto(pageNo, pageSize, merchantId);
        return storeService.queryStoreByPage(queryStoreDto);
    }

    /**
     * 生成商户门店应用二维码
     * @param appId 应用Id 为了获取支付渠道参数
     * @param storeId 门店Id
     *                获取一个商户Id
     * @return
     * @throws BusinessException
     */
    @ApiOperation("生成商户应用门店二维码")
    @GetMapping(value = "/my/apps/{appId}/stores/{storeId}/app-store-qrcode")
    public String createCScanBStoreQRCode(@PathVariable String appId, @PathVariable Long storeId) throws BusinessException, IOException {
        //生成二维码url
        String qrUrl = null; //二维码入口不唯一时放在应用层读取然后选择服务层（非常量） 二维码入口唯一时放在服务层读取（常量）

        Long merchantId = SecurityUtil.getMerchantId();// 获取商户ID
        QrcodeDTO qrcodeDTO = new QrcodeDTO();
        qrcodeDTO.setAppId(appId);
        qrcodeDTO.setMerchantId(merchantId);
        qrcodeDTO.setStoreId(storeId);
        qrcodeDTO.setQrUrl(qrUrl);

        //生成二维码Url链接，入口+地址参数
        String url = transactionService.createQrUrl(qrcodeDTO);
        String qrCode = new QRCodeUtil().createQRCode(url, 200, 200);

        return qrCode;
    }
}
