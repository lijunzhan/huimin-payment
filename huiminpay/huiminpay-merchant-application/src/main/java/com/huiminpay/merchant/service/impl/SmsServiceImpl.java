package com.huiminpay.merchant.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.huiminpay.merchant.service.SmsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;
@Service
@Slf4j
public class SmsServiceImpl implements SmsService {
    @Value("${sms.url}")
    String smsUrl;
    @Value("${sms.effectiveTime}")
    String smsEffectiveTime;

    @Autowired
    RestTemplate restTemplate;
    @Override
    public String sendSms(String mobile) {
        String url = smsUrl+"/generate?effectiveTime="+smsEffectiveTime+"&name=sms";
        //请求体
        Map<String,Object> body = new HashMap<>();
        body.put("mobile",mobile);
        HttpEntity<Map> httpEntity = new HttpEntity(body);
        ResponseEntity<String> exchange = restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
        String body1 = exchange.getBody();
        //通过json解析
        JSONObject jsonObject = JSON.parseObject(body1);

        JSONObject result = jsonObject.getJSONObject("result");
        return result.getString("key");
    }

    @Override
    public Boolean verifyCode(String code, String key) {
        String url = smsUrl+"/verify?name=sms&verificationCode="+code+"&verificationKey="+key;
        ResponseEntity<String> exchange = restTemplate.exchange(url, HttpMethod.POST, null, String.class);
        String body1 = exchange.getBody();
        JSONObject jsonObject = JSON.parseObject(body1);
        return jsonObject.getBoolean("result");
    }
}
