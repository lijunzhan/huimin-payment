package com.huiminpay.merchant.service;

public interface SmsService {
    /**
     * 短信发送
     * @param mobile
     * @return
     */
    public String sendSms(String mobile);

    /**
     * 验证码校验
     * @param code
     * @param key
     * @return
     */
    public Boolean verifyCode(String code,String key);
}
