package com.huiminpay.merchant.controller;

import com.huiminpay.common.cache.domain.BusinessException;
import com.huiminpay.common.cache.domain.CommonErrorCode;
import com.huiminpay.merchant.common.SecurityUtil;
import com.huiminpay.transaction.api.dto.PayChannelParamDTO;
import com.huiminpay.transaction.api.dto.PlatformChannelDTO;
import com.huiminpay.transaction.api.service.PlatformChannelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(value = "支付渠道参数相关操作",tags = "支付渠道参数相关操作",description = "2+2=5")
public class payController {
    @Reference
    PlatformChannelService platformChannelService;


    @GetMapping("/my/platform-channels")
    @ApiOperation("查询平台下所有服务")
    public List<PlatformChannelDTO> listPlatformChannels(){
        return platformChannelService.findAllPlatformChannes();
    }


    @PostMapping("/my/apps/{appId}/platform-channels")
    @ApiOperation("商户应用绑定服务/（开启服务）")
    public Object openService(@PathVariable String appId,@RequestParam("platformChannelCodes")String platformChannel){
        return platformChannelService.openService(appId,platformChannel);
    }

    @ApiOperation("查询服务是否被绑定")
    @GetMapping("/my/merchants/apps/platformchannels")
    public int queryAppBindPlatformChannel(@RequestParam(required = false) String appId,  String platformChannel){
        if (StringUtils.isEmpty(appId)){
            throw new BusinessException(CommonErrorCode.E_100101);
        }
        return platformChannelService.queryAppBindPlatformChannel(appId,platformChannel);
    }

    @ApiOperation("查询服务类型下的支付渠道")
    @GetMapping(value="/my/pay-channels/platform-channel/{platformChannelCode}")
    public Object queryAppBindPlatformChannel(@PathVariable("platformChannelCode") String platformChannelCode){

        return platformChannelService.queryPlatformChannels(platformChannelCode);

    }

    @ApiOperation("商户保存支付渠道参数")
    @RequestMapping(value = "/my/pay-channel-params",
                    method = {RequestMethod.POST,RequestMethod.PUT})
    public Object createPayChannelParam(@RequestBody PayChannelParamDTO payChannelParam){
        //1.非空判断
        if (StringUtils.isEmpty(payChannelParam.getAppId())){
            throw new BusinessException(CommonErrorCode.E_200202);
        }
        if (StringUtils.isEmpty(payChannelParam.getParam())){
            throw new BusinessException(CommonErrorCode.E_200202);
        }
        if (StringUtils.isEmpty(payChannelParam.getPlatformChannelCode())){
            throw new BusinessException(CommonErrorCode.E_200202);
        }
        if (StringUtils.isEmpty(payChannelParam.getPayChannel())){
            throw new BusinessException(CommonErrorCode.E_200202);
        }

        //2.调用保存或者更新

        Long merchantId = SecurityUtil.getMerchantId(); //Bearer JTdCJTIybWVyY2hhbnRJZCUyMiUzQTE0MDEwODU1OTU1MDA5NDEzMTMlN0Q=
        payChannelParam.setMerchantId(merchantId);
        platformChannelService.savePayChannelParam(payChannelParam);

        return "操作成功";
    }

    /**
     * 多条查询
     */
    @ApiOperation("获取指定应用指定服务类型下所包含的原始支付渠道参数列表")
    @GetMapping(value = "/my/pay-channel-params/apps/{appId}/platform-channels/{platformChannel}")
    public List<PayChannelParamDTO> queryPayChannelParams(@PathVariable String appId, @PathVariable String platformChannel){

        return platformChannelService.queryPayChannelParams(appId,platformChannel);
    }

    /**
     * 单条查询
     * @param appId
     * @param platformChannel
     * @param payChannel
     * @return
     */
    @ApiOperation("查询单个应用下的单个平台服务下的支付通道参数")
    @GetMapping(value = "/my/pay-channel-params/apps/{appId}/platform-channels/{platformChannel}/pay-channels/{payChannel}")
    public PayChannelParamDTO queryPayChannelParam(@PathVariable String appId,
                                                   @PathVariable String platformChannel,
                                                   @PathVariable String payChannel){
        return platformChannelService.queryPayChannelParam(appId,platformChannel,payChannel);
    }
}
