package com.huiminpay.merchant.controller;

import com.huiminpay.merchant.api.dto.StaffDTO;
import com.huiminpay.merchant.api.service.IStaffservice;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(value = "商户下员工操作",tags = "商户下员工操作",description = "123123")
public class StaffController {

    @Reference
    IStaffservice staffservice;


    @ApiOperation("商户下新增员工")
    @PostMapping("/staff/add")
    public Object addStafftoMerchant(@RequestBody StaffDTO staffDTO){
        staffservice.addStafftoMerchant(staffDTO);
        return staffDTO;
    }
    @ApiOperation("编辑商户下员工")
    @PostMapping("/staff/add1")
    public Object updataStafftoMerchant(@RequestBody StaffDTO staffDTO){
        staffservice.updataStafftoMerchant(staffDTO);
        return staffDTO;
    }
    @ApiOperation("删除商户下所属员工")
    @PostMapping("/staff/add2")
    public void deleteStafftoMerchant(Long id){
        staffservice.deleteStafftoMerchant(id);
    }
}
