package com.huiminpay.merchant.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel
public class MerchantVo {
    /**
     * 主键
     */
    @ApiModelProperty("商户id")
//   @JsonSerialize(using = ToStringSerializer.class)// 解决long类型超过17位前端出现精度丢失  （局部配置应用）
    private Long id;

    /**
     * 商户名称
     */
    @ApiModelProperty("企业名称")
    private String merchantName;

    /**
     * 企业编号
     */
    @ApiModelProperty("企业编号")
    private String merchantNo;

    /**
     * 企业地址
     */
    @ApiModelProperty("企业地址")
    private String merchantAddress;

    /**
     * 商户类型
     */
    @ApiModelProperty("商户类型")
    private String merchantType;

    /**
     * 营业执照（企业证明）
     */
    @ApiModelProperty("营业执照")
    private String businessLicensesImg;

    /**
     * 法人身份证正面照片
     */
    @ApiModelProperty("法人身份证正面")
    private String idCardFrontImg;

    /**
     * 法人身份证反面照片
     */
    @ApiModelProperty("法人身份证反面")
    private String idCardAfterImg;

    /**
     * 联系人姓名
     */
    @ApiModelProperty("联系人")
    private String username;

    /**
     * 联系人手机号(关联统一账号)
     */
    @ApiModelProperty("手机号,关联统一账号")
    private String mobile;

    /**
     * 联系人地址
     */
    @ApiModelProperty("联系人地址")
    private String contactsAddress;

    /**
     * 审核状态 0-未申请,1-已申请待审核,2-审核通过,3-审核拒绝
     */
    @ApiModelProperty("审核状态,0‐未申请,1‐已申请待审核,2‐审核通过,3‐审核拒绝")
    private String auditStatus;

    /**
     * 租户ID,关联统一用户
     */
    @ApiModelProperty("租户ID")
    private Long tenantId;

    @ApiModelProperty("用户密码")
    private String password;

}
