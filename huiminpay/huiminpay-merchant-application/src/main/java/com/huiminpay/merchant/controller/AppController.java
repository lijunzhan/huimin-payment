package com.huiminpay.merchant.controller;

import com.huiminpay.common.cache.domain.BusinessException;
import com.huiminpay.common.cache.domain.CommonErrorCode;
import com.huiminpay.merchant.api.dto.AppDTO;
import com.huiminpay.merchant.api.service.IAppservice;
import com.huiminpay.merchant.common.SecurityUtil;
import com.huiminpay.merchant.convert.AppVoConvert;
import com.huiminpay.merchant.vo.AppVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(value = "应用操作相关接口",tags = "应用操作相关接口",description = "操作CRUD")
public class AppController {
    @Reference
    IAppservice appservice;

    @ApiOperation("商户创建应用")
    @PostMapping(value = "/my/apps")
    public Object creatApp(@RequestBody AppVo appVo){
        //1.非空校验
        if (StringUtils.isEmpty(appVo.getAppName())){
            throw new BusinessException(CommonErrorCode.E_200001);
        }
        //2.对象转换
        AppDTO appDTO = AppVoConvert.INSTANCE.vo2dto(appVo);

        //3.保存到数据库（获取当前登录商户Id）
        Long merchantId = SecurityUtil.getMerchantId();
        appDTO.setMerchantId(merchantId);

        return appservice.createApp(appDTO);
    }
    @ApiOperation("商户应用编辑")
    @PutMapping("/my/apps")
    public Object apps(@RequestBody AppVo appVo){
        return null;
    }

    @ApiOperation("根据业务Id查询(appId)")
    @GetMapping(value = "/my/apps/{appId}")
    public AppVo myApp(@PathVariable String appId){
        AppDTO appDTO = appservice.myapp(appId);
        return AppVoConvert.INSTANCE.dto2vo(appDTO);
    }

    @ApiOperation("商户查询名下应用")
    @GetMapping(value = "/my/apps")
    public Object myApp(){
        Long merchantId = SecurityUtil.getMerchantId();
        List<AppDTO> appDTOs = appservice.myApps(merchantId);
        return AppVoConvert.INSTANCE.listdto2listvo(appDTOs);
    }
}
