package com.huiminpay.merchant.convert;

import com.huiminpay.merchant.api.dto.MerchantDTO;
import com.huiminpay.merchant.vo.MerchantVo;
import com.huiminpay.merchant.vo.RegMchVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface MerchantConvert {
    //定义MerchantConvert转换类，使用@Mapper注解快速实现对象转换
    MerchantConvert INSTANCE = Mappers.getMapper(MerchantConvert.class);

    MerchantDTO vo2dto(RegMchVo regMchVo);

    MerchantDTO vo2dto(MerchantVo merchantVo);

    MerchantVo dto2vo(MerchantDTO merchantDTO);
}
