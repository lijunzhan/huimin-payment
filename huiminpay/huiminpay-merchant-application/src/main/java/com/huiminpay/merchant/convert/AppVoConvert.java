package com.huiminpay.merchant.convert;

import com.huiminpay.merchant.api.dto.AppDTO;
import com.huiminpay.merchant.vo.AppVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface AppVoConvert  {

    AppVoConvert INSTANCE = Mappers.getMapper(AppVoConvert.class); //相当于new了一个实现类

    AppDTO vo2dto(AppVo app);

    AppVo dto2vo(AppDTO appDTO);

    List<AppVo> listdto2listvo(List<AppDTO> list);
}
