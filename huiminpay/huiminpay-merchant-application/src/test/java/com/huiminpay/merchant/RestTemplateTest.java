package com.huiminpay.merchant;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@SpringBootTest
@RunWith(SpringRunner.class)
@Slf4j
public class RestTemplateTest {

    @Autowired
    RestTemplate restTemplate;


    @Test
    //测试使用restTemplate作为http客户端向http服务端发送请求   使用者为客户端  网站为服务端  通过Java程序模拟浏览请求网站
    public void gethtml(){
        //通过java远程请求百度网站拿到百度地址
        //指定url
        String url = "http://www.baidu.com/";
        //向url发送http请求，得到响应结果
        ResponseEntity<String> forEntity = restTemplate.getForEntity(url, String.class);
        //得到结果
        String body = forEntity.getBody();
        System.out.println(body);
    }

    //使用RestTemplate向验证码发送请求，获取验证码
    //http://localhost:56085/sailing/generate?effectiveTime=600&name=sms
    @Test
    public void getSmsCode(){
        String url = "http://localhost:56085/sailing/generate?effectiveTime=600&name=sms";
        //请求体
        Map<String,Object> body = new HashMap<>();
        body.put("mobile","1088888");
        //请求头
        HttpHeaders httpHeaders = new HttpHeaders();
        //指定Content-type: application/json
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        //请求信息 传入body，header
        HttpEntity httpEntity = new HttpEntity(body,httpHeaders);
        //向url发起请求得到数据
        ResponseEntity<Map> exchange = restTemplate.exchange(url, HttpMethod.POST, httpEntity, Map.class);
        //快速输出日志
        log.info("请求验证码服务，得到响应:{}", JSON.toJSON(exchange));
        Map bodyMap = exchange.getBody();
        System.out.println(bodyMap);
        //从bodtmap中获取result  result是json数据 可以转换成map对象
        Map result = (Map) bodyMap.get("result");
        //从result中获取key
        String key = (String) result.get("key");
        System.out.println(key);
    }
}
