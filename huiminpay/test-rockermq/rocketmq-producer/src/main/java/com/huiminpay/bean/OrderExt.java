package com.huiminpay.bean;

import java.io.Serializable;
import java.time.LocalDateTime;

public class OrderExt implements Serializable {
    private String id;

    private LocalDateTime createTime;

    private Long money;

    private String title;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public Long getMoney() {
        return money;
    }

    public void setMoney(Long money) {
        this.money = money;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "OrderExt{" +
                "id='" + id + '\'' +
                ", createTime=" + createTime +
                ", money=" + money +
                ", title='" + title + '\'' +
                '}';
    }
}
