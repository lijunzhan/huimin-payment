package com.huiminpay.consumer;

import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

@Component
@RocketMQMessageListener(topic = "FHS-MQ",consumerGroup = "demo-consumer-group")
public class ConsumerSimple implements RocketMQListener<String> {

    //接手到消息调用此方法
    @Override
    public void onMessage(String s) {
        System.out.println("收到消息["+s+"]");
    }
}
