package com.huiminpay.consumer;

import com.huiminpay.bean.OrderExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@RocketMQMessageListener(topic = "FHS-EXT",consumerGroup = "demo-consumer-group1")
public class ConsumerExt implements RocketMQListener<OrderExt> {

    //接手到消息调用此方法
    @Override
    public void onMessage(OrderExt s) {
        System.out.println(LocalDateTime.now());
        System.out.println("收到消息["+s+"]");
    }
}
