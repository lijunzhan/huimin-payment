package com.huiminpay.mq;

import com.huiminpay.bean.OrderExt;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

/**
 * rockermq发送消息类
 */
@Component
public class ProducerSimple {
    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    /**
     * 发送同步消息
     * @param topic
     * @param msg
     */
    public void sendSyncMsg(String topic,String msg){
        rocketMQTemplate.syncSend(topic,msg);
    }

    /**
     * 发送异步消息
     * @param topic
     * @param msg
     */
    public void sendAyncMsg(String topic,String msg){
        //异步发送增加一个回调方法
        rocketMQTemplate.asyncSend(topic, msg, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                //返回正常回调
                System.out.println(sendResult);
            }

            @Override
            public void onException(Throwable throwable) {
                //回调异常时
                System.out.println(throwable.getMessage());
            }
        });
    }

    /**
     * 自定义消息
     * @param topic
     * @param orderExt
     */
    public void sendEXTMsg(String topic, OrderExt orderExt){
        rocketMQTemplate.convertAndSend(topic,orderExt);
    }

    /**
     * 延迟消息
     * @param topic
     * @param orderExt
     */
    public void sendMsgDelay(String topic, OrderExt orderExt){
        //发送同步消息，消息内容将orderExt转为json
        Message<OrderExt> message = MessageBuilder.withPayload(orderExt).build();
        //指定发送超时时间（毫秒）和延迟等级
        this.rocketMQTemplate.syncSend(topic,message,1000,3);
    }





}
