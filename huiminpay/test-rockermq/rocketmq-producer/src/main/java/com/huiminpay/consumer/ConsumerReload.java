package com.huiminpay.consumer;

import com.huiminpay.bean.OrderExt;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@RocketMQMessageListener(topic = "FHS-RE_EXT",consumerGroup = "demo-consumer-group2")
public class ConsumerReload implements RocketMQListener<MessageExt> {

    //接手到消息调用此方法
    @Override
    public void onMessage(MessageExt messageExt) {
        System.out.println(LocalDateTime.now());
        System.out.println("收到消息["+messageExt+"]");

        //收到消息的内容
        byte[] body = messageExt.getBody();
        String cnt = new String(body);//转换成字符串
        System.out.println(cnt);

        int reconsumeTimes = messageExt.getReconsumeTimes();//当前重试的次数
        if (reconsumeTimes>=4){
            System.out.println("不再重试");
            return;
        }
        throw new RuntimeException("测试重试！！");
    }
}
