package com.huiminpay;

import com.huiminpay.bean.OrderExt;
import com.huiminpay.mq.ProducerSimple;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TestMq {
    @Autowired
    private ProducerSimple producerSimple;

    //测试发送同步消息
    @Test
    public void testSendSyncMsg(){
        this.producerSimple.sendSyncMsg("FHS-MQ", "第一条同步消息"+ LocalDateTime.now());
        System.out.println("end...");
    }

    @Test
    public void testSendAyncMsg(){
        this.producerSimple.sendSyncMsg("FHS-MQ", "异步消息"+ LocalDateTime.now());
        System.out.println("end...");
    }

    @Test
    public void testEXTMsg(){
        OrderExt orderExt = new OrderExt();
        orderExt.setMoney(1L);
        orderExt.setTitle("自定义消息");
//        this.producerSimple.sendEXTMsg("FHS-EXT", orderExt);
        orderExt.setCreateTime(LocalDateTime.now());
        producerSimple.sendMsgDelay("FHS-EXT",orderExt);
        System.out.println("end...");
    }

    @Test
    public void testReEXTMsg(){
        OrderExt orderExt = new OrderExt();
        orderExt.setMoney(1L);
        orderExt.setTitle("自定义消息");
        orderExt.setCreateTime(LocalDateTime.now());
        producerSimple.sendEXTMsg("FHS-RE_EXT",orderExt);
        System.out.println("end...");
    }
}
