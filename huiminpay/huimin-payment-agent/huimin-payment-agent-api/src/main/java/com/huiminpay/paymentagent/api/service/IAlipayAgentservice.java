package com.huiminpay.paymentagent.api.service;

import com.huiminpay.common.cache.domain.BusinessException;
import com.huiminpay.paymentagent.api.conf.AliConfigParam;
import com.huiminpay.paymentagent.api.dto.AliPayTradeQueryDto;
import com.huiminpay.paymentagent.api.dto.AlipayBean;

public interface IAlipayAgentservice {
    /**
     * 调用支付宝手机网站支付
     * @param aliConfigParam
     * @param alipayBean
     * @return 支付宝请求html
     */
    String doAlipayWapApi(AliConfigParam aliConfigParam, AlipayBean alipayBean) throws BusinessException;

    /**
     * 根据订单号查询支付宝详情
     * @param aliConfigParam
     * @param outTradeNo
     * @return
     * @throws BusinessException
     */
    AliPayTradeQueryDto queryAliPayOrderByTraderNo(AliConfigParam aliConfigParam, String outTradeNo)throws BusinessException;
}
