package com.huiminpay.paymentagent.api.dto;

import lombok.Data;

import java.io.Serializable;
@Data
public class AliPayTradeQueryDto implements Serializable {
    private String tradeNo;
    private String outTradeNo;
    private String tradeStatus;

}
