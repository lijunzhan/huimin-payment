package com.huiminpay.paymentagent.service.impl;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeQueryModel;
import com.alipay.api.domain.AlipayTradeWapPayModel;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.request.AlipayTradeWapPayRequest;
import com.alipay.api.response.AlipayTradeQueryResponse;
import com.alipay.api.response.AlipayTradeWapPayResponse;
import com.huiminpay.common.cache.domain.BusinessException;
import com.huiminpay.common.cache.domain.CommonErrorCode;
import com.huiminpay.paymentagent.api.conf.AliConfigParam;
import com.huiminpay.paymentagent.api.dto.AliPayTradeQueryDto;
import com.huiminpay.paymentagent.api.dto.AlipayBean;
import com.huiminpay.paymentagent.api.service.IAlipayAgentservice;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Service;

@Service
@Slf4j
public class AlipayAgentserviceImpl implements IAlipayAgentservice {

    @Override
    public String doAlipayWapApi(AliConfigParam aliConfigParam, AlipayBean alipayBean)  throws BusinessException {
        /**
         * 网关地址
         */
        String serverUrl = aliConfigParam.getUrl();
        /**
         * 应用ID
         */
        String appId = aliConfigParam.getAppId();
        /**
         * 应用私钥
         */
        String privateKey = aliConfigParam.getRsaPrivateKey();
        String format = aliConfigParam.getFormat();
        String charset = aliConfigParam.getCharest();
        /**
         * 支付宝公钥
         */
        String alipayPublicKey = aliConfigParam.getAlipayPublicKey();
        String signTyep = aliConfigParam.getSigntype();

        AlipayClient alipayClient = new DefaultAlipayClient(serverUrl,appId,privateKey,format,charset,alipayPublicKey,signTyep);
        AlipayTradeWapPayRequest request = new AlipayTradeWapPayRequest();
        if(StringUtils.isNotEmpty(aliConfigParam.getNotifyUrl())){
            request.setNotifyUrl(aliConfigParam.getNotifyUrl());//设置异步通知地址
        }


        AlipayTradeWapPayModel alipayTradePayModel = new AlipayTradeWapPayModel();

        /**
         * 订单标题
         */
        alipayTradePayModel.setSubject(alipayBean.getSubject());
        /**
         * 订单号
         */
        alipayTradePayModel.setOutTradeNo(alipayBean.getOutTradeNo());
        /**
         * 订单金额
         */
        alipayTradePayModel.setTotalAmount(alipayBean.getTotalAmount());
        alipayTradePayModel.setQuitUrl(aliConfigParam.getReturnUrl()+"/"+alipayBean.getOutTradeNo());
        alipayTradePayModel.setProductCode("QUICK_WAP_PAY");
        if (StringUtils.isNotEmpty(alipayBean.getExpireTime())){
            alipayTradePayModel.setTimeoutExpress(alipayBean.getExpireTime());
        }



        request.setBizModel(alipayTradePayModel);
        try {
            AlipayTradeWapPayResponse response = alipayClient.pageExecute(request);
            if(response.isSuccess()){
                return response.getBody();
            }

        } catch (AlipayApiException e) {
            e.printStackTrace();
            throw new BusinessException(CommonErrorCode.UNKOWN);
        }

        throw new BusinessException(CommonErrorCode.UNKOWN);
    }

    /**
     * 根据订单号查询支付宝详情
     *
     * @param aliConfigParam
     * @param outTradeNo
     * @return
     * @throws BusinessException
     */
    @Override

    public AliPayTradeQueryDto queryAliPayOrderByTraderNo(AliConfigParam aliConfigParam, String outTradeNo) throws BusinessException {
      //网关地址
        String serverUrl = aliConfigParam.getUrl();

         //应用ID
        String appId = aliConfigParam.getAppId();

         //应用私钥
        String privateKey = aliConfigParam.getRsaPrivateKey();
        String format = aliConfigParam.getFormat();
        String charset = aliConfigParam.getCharest();

         //支付宝公钥
        String alipayPublicKey = aliConfigParam.getAlipayPublicKey();
        String signTyep = aliConfigParam.getSigntype();

        AlipayClient alipayClient = new DefaultAlipayClient(serverUrl,appId,privateKey,format,charset,alipayPublicKey,signTyep);
        AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();
        AlipayTradeQueryModel model = new AlipayTradeQueryModel();
        model.setTradeNo(outTradeNo);
        request.setBizModel(model);
        try {
            AlipayTradeQueryResponse execute = alipayClient.execute(request);
            if (execute.isSuccess()){
                AliPayTradeQueryDto aliPayTradeQueryDto = new AliPayTradeQueryDto();
                aliPayTradeQueryDto.setOutTradeNo(outTradeNo);
                aliPayTradeQueryDto.setTradeNo(execute.getTradeNo());
                aliPayTradeQueryDto.setTradeStatus(execute.getTradeStatus());
                return aliPayTradeQueryDto;
            }else {
                log.info("[]",execute);
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }

        throw new BusinessException(CommonErrorCode.UNKOWN);
    }
}
