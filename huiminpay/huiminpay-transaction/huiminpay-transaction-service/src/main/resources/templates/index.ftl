<!DOCTYPE html>
<html>
<head>
    <meta charset="utf‐8">
    <title>Hello World!</title>
</head>
<body>
Hello ${User}!

<p>
    Our latest Product
    <a href="${latestProduct.url}">${latestProduct.name}</a>
</p>

//list遍历
<#list users as obj >
      姓名:${obj.name}，年龄:${obj.age},财富：${obj.money}
    //非空校验??
    <#if obj.bf ??>
        男友：${obj.bf.name}
        //name1！ 代表name1这个属性不存在 后边用’‘指定注释或者默认值
        ${obj.bf.name1!'不存在'}
    </#if>
    <br>
</#list>

//map遍历
<#list maps? keys as key >
    姓名:${maps[key].name}，年龄:${maps[key].age},财富：${maps[key].money}
    //非空校验??
    <#if maps[key].bf ??>
        年龄:${maps[key].bf.age},de男友：${maps[key].bf.name}
        //name1！ 代表name1这个属性不存在 后边用’‘写默认值内容
        ${maps[key].bf.asdasd!'不存在'}
    </#if>
    <br>
</#list>
</body>
</html>