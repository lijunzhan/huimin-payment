package com.huiminpay.transaction;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.OkHttp3ClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.util.List;

@SpringBootApplication
@EnableDiscoveryClient
public class TransactionBootstrap {
    public static void main(String[] args) {
        SpringApplication.run(TransactionBootstrap.class,args);
    }

//    @Bean
//        //RestTemplate 初始化
//    public RestTemplate restTemplate(){
//        //通过构造参数将okhttp传给Resttemplate
//        RestTemplate restTemplate = new RestTemplate(new OkHttp3ClientHttpRequestFactory());
//        //得到消息转换器
//        List<HttpMessageConverter<?>> messageConverters = restTemplate.getMessageConverters();
//        //指定StringHttpMessageConverter消息转换器的字符集为UTF-8
//        messageConverters.set(1,new StringHttpMessageConverter(StandardCharsets.UTF_8));
//        restTemplate.setMessageConverters(messageConverters);
//        return restTemplate;
//    }
}