package com.huiminpay.transaction.mq;

import com.alibaba.fastjson.JSON;
import com.huiminpay.paymentagent.api.conf.AliConfigParam;
import com.huiminpay.paymentagent.api.dto.AliPayTradeQueryDto;
import com.huiminpay.paymentagent.api.service.IAlipayAgentservice;
import com.huiminpay.transaction.api.dto.OrderResultDTO;
import com.huiminpay.transaction.api.dto.PayChannelParamDTO;
import com.huiminpay.transaction.api.dto.PayOrderMqDto;
import com.huiminpay.transaction.api.service.ITransactionService;
import com.huiminpay.transaction.api.service.PlatformChannelService;
import com.huiminpay.transaction.entity.PayChannelParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@RocketMQMessageListener(topic = MqConst.PAY_ORDER_LISTENER,consumerGroup = "pay-consumer-group")
@Slf4j
public class OrderListenerMq implements RocketMQListener<MessageExt> {
    @Reference
    ITransactionService transactionService;

    @Reference
    IAlipayAgentservice alipayAgentservice;

    @Reference
    PlatformChannelService platformChannelService;
    //接手到消息调用此方法
    @Override
    public void onMessage(MessageExt messageExt) {
        System.out.println(LocalDateTime.now());
        System.out.println("收到消息["+messageExt+"]");

        //收到消息的内容
        byte[] body = messageExt.getBody();
        String cnt = new String(body);//转换成字符串
        System.out.println(cnt);
        PayOrderMqDto payOrderMqDto = JSON.parseObject(cnt, PayOrderMqDto.class);
        String outTradeNo = payOrderMqDto.getOutTradeNo();
        //查询自建系统的订单信息
        OrderResultDTO orderResultDTO = transactionService.selectPayOrder(outTradeNo);
        if (orderResultDTO.getTradeState().equals("2")
            ||orderResultDTO.getTradeState().equals("3")){
            log.info("[当前订单已支付：{}]"+cnt);
            return;
        }
        //2.判断支付类型
        PayChannelParamDTO payChannelParamDTO =platformChannelService.queryPayChannelParam(orderResultDTO.getAppId(),
                orderResultDTO.getChannel(),orderResultDTO.getPayChannel());
        String param = payChannelParamDTO.getParam();
        String payChannel = payOrderMqDto.getPayChannel();
        if (payChannel.equals("ALIPAY_WAP")){
            //使用支付宝支付
            //3.调用支付宝代理服务
            AliConfigParam aliConfigParam = JSON.parseObject(param,AliConfigParam.class);
            aliConfigParam.setCharest("UTF-8");
            AliPayTradeQueryDto aliPayTradeQueryDto = alipayAgentservice.queryAliPayOrderByTraderNo(aliConfigParam,outTradeNo);
            String trade_status = aliPayTradeQueryDto.getTradeStatus();
            //WAIT_BUYER_PAY 交易创建等待买家付款
            if (trade_status.equals("WAIT_BUYER_PAY")){
                //订单未支付
                int reconsumeTimes = messageExt.getReconsumeTimes();
                if (reconsumeTimes>=8){
                    log.info("[当前订单监听结束：{}]"+cnt);
                    return;
                }
                throw new RuntimeException("消费重试！！第"+reconsumeTimes+"次");
            }else {
                String tradeState = null; //交易支付状态
                if(trade_status.equals("TRADE_CLOSED")){
                    //交易关闭
                    tradeState = "4";
                }else if(trade_status.equals("TRADE_SUCCESS")){
                    tradeState = "2";
                }else if(trade_status.equals("TRADE_FINISHED")){
                    tradeState = "3"; //处理完成
                }

                orderResultDTO.setTradeState(tradeState);//更新订单
                orderResultDTO.setOutTradeNo(aliPayTradeQueryDto.getTradeNo());
                orderResultDTO.setPaySuccessTime(LocalDateTime.now());
                transactionService.updatePayOrderByTradeNo(orderResultDTO);
            }
        }else if (payChannel.equals("WX_JSAPI")){
            //使用微信支付，调用微信接口
        }

        int reconsumeTimes = messageExt.getReconsumeTimes();//当前重试的次数
        if (reconsumeTimes>=8){
            log.info("[当前监听结束：{}]"+cnt);
            System.out.println("不再重试");
            return;
        }
        throw new RuntimeException("消费重试！！第"+reconsumeTimes+"次");
    }
}
