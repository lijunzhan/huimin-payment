package com.huiminpay.transaction.controller;

import com.huiminpay.transaction.common.util.Product;
import com.huiminpay.transaction.common.util.UserBean;
import org.apache.catalina.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("free")
public class TestFreemakerController {
    //freemaker快速入门
    @GetMapping("/index")
    public String index(Model model){
        model.addAttribute("User","xiao li");

        //添加产品
        Product product = new Product();
        product.setUrl("http://127.0.0.1/1.jpg");
        product.setName("绿色鼠标");

        //通过model放入模板中   获取对象的内容
        model.addAttribute("latestProduct",product);

        //获取list的内容
        UserBean zhangsan = new UserBean();
        zhangsan.setName("张三");
        zhangsan.setAge(18);
        zhangsan.setMoney(0L);

        UserBean lisi = new UserBean();
        lisi.setName("lis");
        lisi.setAge(88);
        lisi.setMoney(999999999999999L);
        lisi.setBf(zhangsan);

        List<UserBean> userBeans = new ArrayList<>();
        userBeans.add(zhangsan);
        userBeans.add(lisi);

        //获取map的内容
        Map<String,UserBean> map = new HashMap<>();
        map.put("zhangsan",zhangsan);
        map.put("lisi",lisi);

        model.addAttribute("users",userBeans);
        model.addAttribute("maps",map);
        return "index";

    }

}
