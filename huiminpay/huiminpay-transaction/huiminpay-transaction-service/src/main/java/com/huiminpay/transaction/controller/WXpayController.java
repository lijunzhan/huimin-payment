package com.huiminpay.transaction.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import sun.rmi.runtime.Log;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;

@Controller
@Slf4j
public class WXpayController {
    String appId = "wxb899fab434605ed7",appsecret = "4f89b5d8bf82e5160212a97e1613cb90";



    @RequestMapping("wx/login")
    public void login(HttpServletResponse response) throws IOException {
        //用户同意授权 获取code
        String redirect_uri = "http://r4dhye.natappfree.cc/transaction/wx/code";//内网穿透地址
        redirect_uri = URLEncoder.encode(redirect_uri,"UTF-8");
        //获取
        String url ="https://open.weixin.qq.com/connect/oauth2/authorize?appid=APPID&redirect_uri=REDIRECT_URI&response_type=code&scope=SCOPE&state=STATE#wechat_redirect";
        //替换数据
        url = url.replaceAll("APPID",appId)
                .replaceAll("REDIRECT_URI",redirect_uri)
                .replaceAll("SCOPE","snsapi_userinfo");
        response.sendRedirect(url);
    }

    @RequestMapping("wx/code")
    @ResponseBody
    public Object code(HttpServletRequest request){
        //通过code换取网页授权access_token
        String code = request.getParameter("code");
        log.info("[CODE:{}]",code);

        String url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";
        url = url.replaceAll("APPID",appId).replaceAll("SECRET",appsecret).replaceAll("CODE",code);
        RestTemplate restTemplate = new RestTemplate();
        String forObject = restTemplate.getForObject(url, String.class);
        if (forObject.contains("errcode")){
            System.out.println("获取令牌失败"+forObject);
            return "获取令牌失败"+forObject;
        }
        JSONObject jsonObject = JSON.parseObject(forObject);
        String access_token = jsonObject.getString("access_token");
        String openid = jsonObject.getString("openid");
           //根据令牌获取用户信息
        String userUrl = "https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN";
        userUrl = userUrl.replaceAll("ACCESS_TOKEN", access_token).replaceAll("OPENID", openid);
        String forObject1 = restTemplate.getForObject(userUrl, String.class);
        log.info("[获取用户信息：{}]",forObject1);
//        JSONObject jsonObject1 = JSON.parseObject(forObject1);
//        String openid1 = jsonObject1.getString("openid");
        return forObject1;
    }
}
