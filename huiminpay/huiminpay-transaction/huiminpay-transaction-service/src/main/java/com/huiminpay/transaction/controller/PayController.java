package com.huiminpay.transaction.controller;

import com.alibaba.fastjson.JSON;
import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import com.huiminpay.common.cache.util.AmountUtil;
import com.huiminpay.common.cache.util.EncryptUtil;
import com.huiminpay.common.cache.util.ParseURLPairUtil;
import com.huiminpay.paymentagent.api.conf.AliConfigParam;
import com.huiminpay.transaction.api.dto.OrderResultDTO;
import com.huiminpay.transaction.api.dto.PayChannelParamDTO;
import com.huiminpay.transaction.api.dto.PayOrderDTO;
import com.huiminpay.transaction.api.service.ITransactionService;
import com.huiminpay.transaction.api.service.PlatformChannelService;
import com.huiminpay.transaction.entity.PlatformChannel;
import com.huiminpay.transaction.vo.OrderConfirmVO;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Controller
@Slf4j
public class PayController {

    @Reference
    ITransactionService transactionService;

    @Reference
    PlatformChannelService platformChannelService;


    @GetMapping("qr/{token}")
    public String entry(@PathVariable String token, HttpServletRequest request) throws Exception {
        //1.解析token数据
        String s = EncryptUtil.decodeUTF8StringBase64(token);
        PayOrderDTO payOrderDTO = JSON.parseObject(s, PayOrderDTO.class);
        log.info("{}",payOrderDTO);

        //2.判断当前请求来自支付宝还是微信（header中user-agent的值来判断）
        String userAgent =request.getHeader("User-Agent");
        if (userAgent != null&&userAgent.contains("AlipayClient")){
            //支付宝客户端支付
                                        //将对象中的对象名转成键值对的形式
            return "forward:/pay-page?"+ ParseURLPairUtil.parseURLPair(payOrderDTO);

        }
        return "forward:/pay-page?"+ ParseURLPairUtil.parseURLPair(payOrderDTO);
    }

    @GetMapping("pay-page")
    public String payPage(){
        return "pay";
    }



    @ApiOperation("支付宝下单")
    @PostMapping("createAliPayOrder")
    public void createAliPayOrder(OrderConfirmVO orderConfirmVO, HttpServletResponse httpServletResponse) throws Exception {
        //用DTO传参
        PayOrderDTO payOrderDTO = new PayOrderDTO();
        BeanUtils.copyProperties(orderConfirmVO,payOrderDTO);

        //转换问题
        String y2F = AmountUtil.changeY2F(orderConfirmVO.getTotalAmount());
        payOrderDTO.setTotalAmount(Integer.valueOf(y2F));
        payOrderDTO.setStoreId(Long.valueOf(orderConfirmVO.getStoreId()));
        String alipayHtml = transactionService.createAliPayOrder(payOrderDTO);
        httpServletResponse.setContentType("text/html;charset=UTF-8");
        httpServletResponse.getWriter().write(alipayHtml);
        httpServletResponse.getWriter().close();
        httpServletResponse.getWriter().flush();
    }

    @ApiOperation("支付宝异步通知")
    @RequestMapping("alipay/notify")
    public void notifyUrl(HttpServletRequest request,HttpServletResponse response) throws AlipayApiException, IOException {
        //获取支付宝请求参数
        Map<String,String[]> parameterMap = request.getParameterMap();
        Map<String,String> map = new HashMap<>();
        parameterMap.keySet().forEach(key->{
            map.put(key,parameterMap.get(key)[0]);
        });
        log.info("{}",map);
        String outTradeNo =  map.get("out_trade_no");
        OrderResultDTO orderResultDTO = transactionService.selectPayOrder(outTradeNo);
        //1.验证签名
        String platformChannel="huimin_c2b",  payChannel="ALIPAY_WAP";//第一种
        //第二种 根据订单号查询订单信息
        PayChannelParamDTO payChannelParamDTO = platformChannelService.queryPayChannelParam(orderResultDTO.getAppId(), platformChannel, payChannel);
        String param = payChannelParamDTO.getParam();
        AliConfigParam aliConfigParam = JSON.parseObject(param,AliConfigParam.class);


        String publicKey = aliConfigParam.getAlipayPublicKey();//支付宝公钥
//        AlipaySignature.rsaCheckV1(map,publicKey,"utf-8");
        boolean b = AlipaySignature.rsaCheckV1(map, publicKey, "utf-8","RSA2");
        if(!b){
            response.getWriter().write("error");
        }
        //2.更新状态
        //支付宝状态转系统状态  //交易状态支付状态,0-订单生成,2-支付成功,3-业务处理完成 4-交易关闭
        String trade_status = map.get("trade_status");//交易状态
        String tradeState = null; //交易支付状态
        if(trade_status.equals("TRADE_CLOSED")){
            //交易关闭
            tradeState = "4";
        }else if(trade_status.equals("TRADE_SUCCESS")){
            tradeState = "2";
        }else if(trade_status.equals("TRADE_FINISHED")){
            tradeState = "3"; //处理完成
        }

        OrderResultDTO orderResultDTO2 = new OrderResultDTO();
        orderResultDTO2.setTradeState(tradeState);//更新订单
        orderResultDTO2.setOutTradeNo(outTradeNo);
        orderResultDTO2.setPaySuccessTime(LocalDateTime.now());
        transactionService.updatePayOrderByTradeNo(orderResultDTO2);
        response.getWriter().write("success");
        //3.如果成功就写success
        response.getWriter().flush();
        response.getWriter().close();
    }

    @ApiOperation("支付宝同步通知")
    @RequestMapping("alipay/sucess/{tradeNo}")
    public String notifyUrl(@PathVariable String tradeNo, Model model){
        //设置超时时间
        OrderResultDTO orderResultDTO = transactionService.selectPayOrder(tradeNo);
        String msg = "支付失败！", msg_notice = "请用支付宝再次扫码";
        if (orderResultDTO!=null){
            String tradeState = orderResultDTO.getTradeState();
            if (tradeState.equals("2")|| tradeState.equals("3")){
                msg="支付成功";
            }
        }
        model.addAttribute("msg",msg);
        model.addAttribute("msg_notice",msg_notice);

        return "pay_error";
    }
}
