package com.huiminpay.transaction.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huiminpay.transaction.api.dto.PayChannelDTO;
import com.huiminpay.transaction.entity.AppPlatformChannel;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 说明了应用选择了平台中的哪些支付渠道 Mapper 接口
 * </p>
 *
 * @author author
 * @since 2020-09-15
 */
@Repository
public interface AppPlatformChannelMapper extends BaseMapper<AppPlatformChannel> {
    @Select("SELECT pc.* FROM `pay_channel` pc, `platform_pay_channel` ppc WHERE pc.`CHANNEL_CODE` = ppc.`PAY_CHANNEL`\n" +
            "AND ppc.`PLATFORM_CHANNEL` = #{platformChannelCode}")
    List<PayChannelDTO> queryPlatformChannels(String platformChannelCode);
}
