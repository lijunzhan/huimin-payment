package com.huiminpay.transaction.service;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.huiminpay.common.cache.domain.BusinessException;
import com.huiminpay.common.cache.domain.CommonErrorCode;
import com.huiminpay.common.cache.util.AmountUtil;
import com.huiminpay.common.cache.util.EncryptUtil;
import com.huiminpay.common.cache.util.IdWorkerUtils;
import com.huiminpay.merchant.api.dto.StoreDTO;
import com.huiminpay.merchant.api.service.IAppservice;
import com.huiminpay.merchant.api.service.IStoreService;
import com.huiminpay.paymentagent.api.conf.AliConfigParam;
import com.huiminpay.paymentagent.api.dto.AlipayBean;
import com.huiminpay.paymentagent.api.service.IAlipayAgentservice;
import com.huiminpay.transaction.api.dto.*;
import com.huiminpay.transaction.api.service.ITransactionService;
import com.huiminpay.transaction.api.service.PlatformChannelService;
import com.huiminpay.transaction.convert.PayOrderConvert;
import com.huiminpay.transaction.entity.PayOrder;
import com.huiminpay.transaction.mapper.PayOrderMapper;
import com.huiminpay.transaction.mq.MqConst;
import com.huiminpay.transaction.mq.PayProducer;
import com.mysql.cj.jdbc.result.UpdatableResultSet;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.time.LocalDateTime;

//下单接口
@Service
public class TransactionServiceImpl implements ITransactionService {
    @Reference
    IStoreService storeService;

    @Value("${qr.entry}")
    private String qrEntry;

    @Reference
    IAppservice appservice;

    @Autowired
    PayOrderMapper payOrderMapper;

    @Reference
    IAlipayAgentservice alipayAgentservice;

    @Reference
    PlatformChannelService plateFormChannelService;

    @Autowired
    PayProducer payProducer;
    /**
     * 生成二维码入口url地址
     *  入口文件
     * @param qrcodeDTO
     * @return
     */
    @Override
    public String createQrUrl(QrcodeDTO qrcodeDTO) {
        //因为缺少门店信息 所以先查询
        StoreDTO storeDTO = storeService.selectById(qrcodeDTO.getStoreId());
        qrcodeDTO.setSubject(storeDTO.getStoreName());//支付入口的标题
        qrcodeDTO.setBody(storeDTO.getStoreName());//支付入口的内容 用标题先代替

        PayOrderDTO payOrderDTO = new PayOrderDTO();
        BeanUtils.copyProperties(qrcodeDTO,payOrderDTO);
        //拼接url地址
        String key = EncryptUtil.encodeUTF8StringBase64(JSON.toJSONString(payOrderDTO));
        String url = qrEntry+key;

        return url;
    }

    /**
     * 下单
     * @param payOrderDTO
     * @return
     * @throws Exception
     */
    @Override
    public String createAliPayOrder(PayOrderDTO payOrderDTO) throws Exception {
        //1.生成订单
        String trandNO = IdWorkerUtils.getInstance().createUUID();

        String payChannel = "ALIPAY_WAP";
        String channel = "huimin_c2b";
        //2.下单
        saveOrder(payOrderDTO, trandNO, payChannel, channel);

        //3.调用支付宝代理服务

        //3.1业务参数
        String alipayWapHtml = doAlipayH5(payOrderDTO, trandNO, payChannel, channel);

        //4.发送延迟消息(相当于生产者)
        PayOrderMqDto payOrderMqDto = new PayOrderMqDto();
        payOrderMqDto.setOutTradeNo(trandNO);
        payOrderMqDto.setPayChannel(payChannel);
        payProducer.sendMsgDelay(MqConst.PAY_ORDER_LISTENER,payOrderMqDto);

        return alipayWapHtml;
    }

    @Override
    public void updatePayOrderByTradeNo(OrderResultDTO orderResultDTO) {
        PayOrder payOrder = PayOrderConvert.INSTANCE.dto2entity(orderResultDTO);
        UpdateWrapper<PayOrder> qw = new UpdateWrapper<>();
        qw.lambda().eq(PayOrder::getOutTradeNo,orderResultDTO.getOutTradeNo());
        payOrderMapper.update(payOrder,qw);
    }

    @Override
    public OrderResultDTO selectPayOrder(String outTradeNo) {
        QueryWrapper<PayOrder> qw = new QueryWrapper<>();
        qw.lambda().eq(PayOrder::getOutTradeNo,outTradeNo);
        PayOrder payOrder = payOrderMapper.selectOne(qw);

        return PayOrderConvert.INSTANCE.entity2dto(payOrder);
    }

    /**
     * 调用支付宝手机网站支付
     * @param payOrderDTO
     * @param trandNO
     * @param payChannel
     * @param channel
     * @return
     * @throws Exception
     */
    private String doAlipayH5(PayOrderDTO payOrderDTO, String trandNO, String payChannel, String channel) throws Exception {
        AlipayBean alipayBean = new AlipayBean();//获取订单参数
        alipayBean.setBody(payOrderDTO.getBody());
        alipayBean.setOutTradeNo(trandNO);
        String ta = AmountUtil.changeF2Y(String.valueOf(payOrderDTO.getTotalAmount()));// 分转元
        alipayBean.setTotalAmount(ta);//
        alipayBean.setProductCode("QUICK_WAP_PAY");
        alipayBean.setSubject(payOrderDTO.getSubject());
        //3.2公共参数
        //AliConfigParam aliConfigParam = new AliConfigParam();//公共参数
        PayChannelParamDTO payChannelParamDTO = plateFormChannelService.queryPayChannelParam(payOrderDTO.getAppId(), channel, payChannel);
        String param = payChannelParamDTO.getParam();
        if (StringUtils.isEmpty(param)){
            throw new BusinessException(CommonErrorCode.UNKOWN);
        }
        AliConfigParam aliConfigParam = JSON.parseObject(param,AliConfigParam.class);//解析json获取公共参数
        aliConfigParam.setNotifyUrl(" http://192.168.0.106:56050/transaction/alipay/notify");

        alipayBean.setExpireTime("10m");//设置超时时间
        return alipayAgentservice.doAlipayWapApi(aliConfigParam, alipayBean);
    }

    /**
     * 保存支付订单
     * @param payOrderDTO
     * @param trandNO
     * @param payChannel
     * @param channel
     * @return 支付参数
     */
    private PayOrder saveOrder(PayOrderDTO payOrderDTO, String trandNO, String payChannel, String channel) {
        StoreDTO storeDTO = storeService.selectById(payOrderDTO.getStoreId());
        OrderResultDTO orderResultDTO = PayOrderConvert.INSTANCE.request2dto(payOrderDTO);
        orderResultDTO.setTradeNo(trandNO); //第三方聚合支付订单号
        orderResultDTO.setMerchantId(storeDTO.getMerchantId());//商户id
        orderResultDTO.setPayChannel(payChannel);//支付渠道编码
        orderResultDTO.setChannel(channel);//服务的类型
        orderResultDTO.setOutTradeNo(trandNO);//商户订单号
        orderResultDTO.setCurrency("CNY");//币种
        /**
         *交易状态支付状态,0-订单生成,1-支付中(目前未使用),2-支付成功,3-业务处理完成
         */
        orderResultDTO.setTradeState("0");//支付状态
        orderResultDTO.setCreateTime(LocalDateTime.now());//创建时间
        orderResultDTO.setUpdateTime(LocalDateTime.now());//更新时间
        orderResultDTO.setExpireTime(LocalDateTime.now().plusMinutes(30L));//设置超时时间

        orderResultDTO.setProductCode("QUICK_WAP_PAY");//产品编码

        PayOrder payOrder = PayOrderConvert.INSTANCE.dto2entity(orderResultDTO);
        payOrderMapper.insert(payOrder);
        return payOrder;
    }
}
