package com.huiminpay.transaction.controller;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeWapPayModel;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.response.AlipayTradeQueryResponse;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class TestController {

    @ApiOperation("支付宝根据订单号查询API对接")
    @GetMapping("alipay/select/{OutTradeNo}")
    public Object selectByOrdelId(@PathVariable String OutTradeNo) throws AlipayApiException, IOException{

        //配置公共参数
        String serverUrl = "https://openapi.alipaydev.com/gateway.do";
        String appId = "2021000118601826";
        String privateKey = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCr3q4EDxYSOF2/JSU7TdpAXkJql8jy3XR8KQSevurZ6DDuezDjx1xcyXny21ahLjnDArdcT5oQWoY8z2RyXSR++rLKUQGtQoPzLvmH0RobO6p7AJR6+GTO7NfJ6mc+Wh3ve9rWUIdQqlzWQEfnjKVUerSWj4PPMFj5ox1q3AKFWfr37zQb0cnjJRV0S8SR0RLFd15R1lYqHHywu5Kgs/VpgA6tbhUIoW6XOupSNCm8RP3es7duQ5tb2sFpekynJMXYYo7kdHnE+ZfxeakXceQnMXiPEd2wrgpIZ4IqnUdwd+U7IPz0wHq8E+w97qwHN9jKNxuDtRoCipGkVIzY9eiPAgMBAAECggEAd7AIEQ2Mej3EkyNEDUuHDMJXkDGJoA+2Weyaqg4cMImw4bLqdgVUfc1U3ln+txog4XwXwvIwQOk5iEmJEtdoXgEp1xiYiVbD2utAnGNywbpIW4Yo4IXwdeWb1KInKdXJ+QeQeyTzRqqNIir7p/VbATy+XAcvbG6n15jPGKaEHuZGxXBU34sdgXf5zwolqSgGPJbnNpiWHhXLwROK4OTviOSNsrl3okvpCEPhBdBWjKqJVl0tb6ip7uh+01KL+ZUyM5UDws2sTfRgiI7uT/HAylbStxZ9BAkafVKaOrm0XmcIrwB7jDMqs6rNAR8g+6JqWRGDoQMRqORUtJnmMuexUQKBgQDv5Lcn0gaOyMlvtEf8IRNJYT5u0VHKOHSAVqILN9YozrfUm7HkAEg8FMlaJU94us8ryL5jtOoy4VTfyXA1VC1TRXAsnz66dnwNDBGJbJGH9WkuqD4YhjiWSExerN5tYlEvYe0gWktsFqLhMvznj4knDaEhm+DIDO/QGYQ+O1+fFwKBgQC3aMasHlsitfG/xW79B+l93YKaT816xTQWrUQnXxW+TVFs68sWcMc1nbdaCWbS3l+8r+U1DErgervVHhluQszAUbz7ZHCTjRAwD3wQtVZXOWKTeRH0H5qwnfddjPJpTxqa6aXfPGF29lF/peSrVuZX+CU/wSTngHvSxjH/qqmtSQKBgEV/MvhcRf4TeRfdWIVYZ6f4fgfIqDM3S7jqYCNz150W1fbH7C8rTcHqG7L74BwPsnjyvQVRog9/bAEw8Eriyga3tRmYQKnwLVvyRRpU04xZslEL+IFOURJoe4dp+T4jH3XknreisPrtS96F0PKciJVBw45jBCmwZcEZw0/sD3WdAoGAeNBZ3lL4sXqsTiidnauykKhzOXbL4c05LJ7hwBWQWxTPSDJiStvrPVQwzOxYgRrecYpkL3dzcz8lr5LuKYVzHlQ5aleVGHzZZu8sSE1LHJW+a2pa+zY+TY0gvszzeY8T9WtnSg+aHCTRNoZuhLbvcI8S6lYuGxi1d/f6Q3GF55ECgYEAkIOFac2JNk6SNpcQeV0xC/a+7ya6dlpc6u0iiMxa1MSHJev/2RdGWTpa5b6hW+8Vg5JS1Wd3JHl4nIOpc7H4DFWFpPtxnKhqFuQTAvy848Zjs89+/TIf4MSkTxDo68syw+W9efQDf0CKbYOVzkfCjb4NYXM1jnOvWKmMCB5CmdQ=";
        String format = "json";
        String charset = "UTF-8";
        String alipayPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApClplWFjnIrRSKv6t7l+6A8c+3VsBadNcUNAtlOtgPYy0sv0KvQ0m65aLQfiEvjkkako9kptAZ1avAHMIFO1tcPt6AHsqqjeyaHntDaAUpwRAGEnvbffT6uXNevvatctkbtIfv44i9D49EVKb8em1pQ7Kr2FnS3I8GgCR9/B8XHkDDRaaxrmZFlbygMrd2YHN+yw+Zb7zcfLa52upg5Ha3OjJwpMj8wF8o0Yvj2xctgEceSZfuZY9aXUjvMPabG6E9dSd+6AMPLzBX+bmzu4IT5hGSMvii33CbgOW5BxFs8Ibp/YxRjzDgDzZajGW1J+l+cs9y+ujyj8gCB2PCx5tQIDAQAB";
        String signTyep = "RSA2";

//        AlipayClient alipayClient = new DefaultAlipayClient("https://openapi.alipay.com/gateway.do","app_id","your private_key","json","GBK","alipay_public_key","RSA2");
//        AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();
//        request.setBizContent("{" +
//
//        AlipayTradeQueryResponse response = alipayClient.execute(request);
//        if(response.isSuccess()){
//            System.out.println("调用成功");
//        } else {
//            System.out.println("调用失败");
//        }

        AlipayClient alipayClient = new DefaultAlipayClient(serverUrl,appId,privateKey,format,charset,alipayPublicKey,signTyep);
        AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();

        AlipayTradeWapPayModel alipayTradeWapPayModel = new AlipayTradeWapPayModel();
        alipayTradeWapPayModel.setOutTradeNo(OutTradeNo);
        request.setBizModel(alipayTradeWapPayModel);

        AlipayTradeQueryResponse response = alipayClient.execute(request);
        if(response.isSuccess()){
            System.out.println("调用成功");
            String body = response.getBody();
//            servletResponse.getWriter().write(body);
            System.out.println(body);
        } else {
            System.out.println("调用失败");
        }
        return response;
    }

}
