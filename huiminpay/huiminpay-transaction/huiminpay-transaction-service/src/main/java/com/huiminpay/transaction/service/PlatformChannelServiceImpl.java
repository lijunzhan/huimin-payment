package com.huiminpay.transaction.service;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.huiminpay.common.cache.Cache;
import com.huiminpay.common.cache.domain.BusinessException;
import com.huiminpay.common.cache.domain.CommonErrorCode;
import com.huiminpay.transaction.api.dto.PayChannelDTO;
import com.huiminpay.transaction.api.dto.PayChannelParamDTO;
import com.huiminpay.transaction.api.dto.PlatformChannelDTO;
import com.huiminpay.transaction.api.service.PlatformChannelService;
import com.huiminpay.transaction.convert.PayChannelParamConvert;
import com.huiminpay.transaction.convert.PlatformChannelConvert;
import com.huiminpay.transaction.entity.AppPlatformChannel;
import com.huiminpay.transaction.entity.PayChannelParam;
import com.huiminpay.transaction.entity.PlatformChannel;
import com.huiminpay.transaction.mapper.AppPlatformChannelMapper;
import com.huiminpay.transaction.mapper.PayChannelParamMapper;
import com.huiminpay.transaction.mapper.PlatformChannelMapper;
import org.apache.dubbo.config.annotation.Service;
import org.apache.dubbo.rpc.AppResponse;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class PlatformChannelServiceImpl implements PlatformChannelService {

    @Autowired
    PlatformChannelMapper platformChannelMapper;

    @Autowired
    AppPlatformChannelMapper appPlatformChannelMapper;

    @Autowired
    PayChannelParamMapper payChannelParamMapper;

    @Autowired
    Cache cache;

    private String keyPre = "HUIMIN:PAY:PARAM:";

    @Override
    public List<PlatformChannelDTO> findAllPlatformChannes() {

        List<PlatformChannel> platformChannels = platformChannelMapper.selectList(null);
        return PlatformChannelConvert.INSTANCE.listentity2listdto(platformChannels);
    }

    @Override
    public Object openService(String appId, String platformChannel) throws BusinessException {
        //1.校验唯一性
        Integer integer = getAppBindPlateformChannelCnt(appId, platformChannel);
        if (integer>0){
            throw new BusinessException(CommonErrorCode.E_200003);
        }
        //2.保存数据
        AppPlatformChannel appPlatformChannel = new AppPlatformChannel();
        appPlatformChannel.setAppId(appId);
        appPlatformChannel.setPlatformChannel(platformChannel);
        appPlatformChannelMapper.insert(appPlatformChannel);
        return appPlatformChannel;
    }

    /**
     * 公共代码 应用绑定渠道代码抽取
     * @param appId
     * @param platformChannel
     * @return
     */
    private Integer getAppBindPlateformChannelCnt(String appId, String platformChannel) {
        QueryWrapper<AppPlatformChannel> qw = new QueryWrapper<>();
        qw.lambda().eq(AppPlatformChannel::getAppId,appId)
                .eq(AppPlatformChannel::getPlatformChannel,platformChannel);
        return appPlatformChannelMapper.selectCount(qw);
    }

    /**
     * 查询服务是否被绑定
     * @param appId
     * @param platformChannel
     * @return
     */
    @Override
    public int queryAppBindPlatformChannel(String appId, String platformChannel) {

        return getAppBindPlateformChannelCnt(appId,platformChannel);
    }

    /**
     * 查询服务类型下的支付渠道
     * @param platformChannelCode
     * @return
     */
    @Override
    public List<PayChannelDTO> queryPlatformChannels(String platformChannelCode) {
        return appPlatformChannelMapper.queryPlatformChannels(platformChannelCode);
    }

    @Override
    public void savePayChannelParam(PayChannelParamDTO payChannelParam) throws BusinessException {
        //非空校验

        //1.根据appId和platformChannelCode获取appPlatformChannelId
        AppPlatformChannel appPlatformChannel = getAppPlatformChannel(payChannelParam);
        if (appPlatformChannel == null){
            throw new BusinessException(CommonErrorCode.E_200210);
        }
        Long appPlatformChannelId = appPlatformChannel.getId();


        //2.根据appPlatformChannelId和payChannle的为一项来判断数据控制是否已存在此渠道
        PayChannelParam payChannelParam1 = getPayChannelParam(payChannelParam, appPlatformChannelId);


        //3.不存在时插入，存在时操作为更新
        if (payChannelParam1 == null){
            //插入
            //对象转换
            PayChannelParam payChannelParam2 = PayChannelParamConvert.INSTANCE.dto2entity(payChannelParam);
            payChannelParam2.setAppPlatformChannelId(appPlatformChannelId);
            payChannelParamMapper.insert(payChannelParam2);
        }else {
            //更新
            payChannelParam1.setChannelName(payChannelParam.getChannelName());//通道名更新
            //支付渠道参数
            payChannelParam1.setParam(payChannelParam.getParam());
            //执行更新
            payChannelParamMapper.updateById(payChannelParam1);
        }

        //更新缓存 产品名：模块名：功能名：业务参数 HUIMIN:PAY:PARAM:
        updatcache(payChannelParam);
    }

    private void updatcache(PayChannelParamDTO payChannelParam) {
        String key = keyPre+payChannelParam.getAppId()+":"+payChannelParam.getPlatformChannelCode();
        List<PayChannelParamDTO> payChannelParamDTOS = getPayChannelParamDTOS(payChannelParam.getAppId(),payChannelParam.getPlatformChannelCode());
        String val = JSON.toJSONString(payChannelParamDTOS);
        cache.set(key,val);
    }

    @Override
    public PayChannelParamDTO queryPayChannelParam(String appId, String platformChannel, String payChannel) {
        //从redids中读取缓存  先判断缓存是否存在
        String key = keyPre+appId+":"+platformChannel;
        if (cache.exists(key)){
            //缓存存在
            String s = cache.get(key);
            //用fastjson解析json字符串  读取缓存
            //查询单个所以 先从集合中遍历查询
           List<PayChannelParamDTO> list = JSON.parseArray(s,PayChannelParamDTO.class);
            for (PayChannelParamDTO payChannelParamDTO : list) {
                if (payChannelParamDTO.getPayChannel().equals(payChannel)){
                    return payChannelParamDTO;
                }
            }
        }
        //1.根据appId和platformChannelCode获取appPlatformChannelId
        PayChannelParamDTO payChannelParam = new PayChannelParamDTO();
        payChannelParam.setAppId(appId);
        payChannelParam.setPlatformChannelCode(platformChannel);
        payChannelParam.setPayChannel(payChannel);

        AppPlatformChannel appPlatformChannel = getAppPlatformChannel(payChannelParam);
        if (appPlatformChannel == null){
            throw new BusinessException(CommonErrorCode.E_200210);
        }
        Long appPlatformChannelId = appPlatformChannel.getId();

        //2.根据appPlatformChannelId和payChannle的为一项来判断数据控制是否已存在此渠道
        PayChannelParam payChannelParam1 = getPayChannelParam(payChannelParam, appPlatformChannelId);

        //更新缓存
        updatcache(payChannelParam);
        return PayChannelParamConvert.INSTANCE.entity2dto(payChannelParam1);
    }

    @Override
    public List<PayChannelParamDTO> queryPayChannelParams(String appId, String platformChannel) {
        // 通过redis读取缓存  先判断缓存是否存在
        String key = keyPre+appId+":"+platformChannel;

        if (cache.exists(key)){
            //缓存存在
            String s = cache.get(key);
            //用fastjson解析json字符串  读取缓存
            return JSON.parseArray(s,PayChannelParamDTO.class);
        } else {
            //缓存不存在时 先查询 再存入redis最后读取缓存
            List<PayChannelParamDTO> payChannelParamDTOS = getPayChannelParamDTOS(appId,platformChannel);
            cache.set(key,JSON.toJSONString(payChannelParamDTOS));
            return payChannelParamDTOS;

        }
//        return getPayChannelParamDTOS(appId, platformChannel);
    }

    private List<PayChannelParamDTO> getPayChannelParamDTOS(String appId, String platformChannel) {
        PayChannelParamDTO payChannelParam = new PayChannelParamDTO();
        payChannelParam.setAppId(appId);
        payChannelParam.setPlatformChannelCode(platformChannel);

        //1.根据appId和platformChannelCode获取appPlatformChannelId
        AppPlatformChannel appPlatformChannel = getAppPlatformChannel(payChannelParam);
        if (appPlatformChannel == null){
            throw new BusinessException(CommonErrorCode.E_200210);
        }
        Long appPlatformChannelId = appPlatformChannel.getId();

        //2.根据 appPlatformChannelId查询全部支付渠道参数

        QueryWrapper<PayChannelParam> qw = new QueryWrapper<>();
        qw.lambda().eq(PayChannelParam::getAppPlatformChannelId,appPlatformChannelId);
        List<PayChannelParam> payChannelParams = payChannelParamMapper.selectList(qw);

        return PayChannelParamConvert.INSTANCE.listentity2listdto(payChannelParams);
    }

    /**
     * 查询支付渠道参数
     * @param payChannelParam
     * @param appPlatformChannelId
     * @return
     */
    private PayChannelParam getPayChannelParam(PayChannelParamDTO payChannelParam, Long appPlatformChannelId) {
        QueryWrapper<PayChannelParam> qw = new QueryWrapper<>();
        qw.lambda().eq(PayChannelParam::getAppPlatformChannelId,appPlatformChannelId)
                .eq(PayChannelParam::getPayChannel,payChannelParam.getPayChannel());
        return payChannelParamMapper.selectOne(qw);
    }

    /**
     * 查询应用绑定服务
     * @param payChannelParam
     * @return
     */
    private AppPlatformChannel getAppPlatformChannel(PayChannelParamDTO payChannelParam) {
        QueryWrapper<AppPlatformChannel> qw = new QueryWrapper<>();
        qw.lambda().eq(AppPlatformChannel::getAppId,payChannelParam.getAppId())
                .eq(AppPlatformChannel::getPlatformChannel,payChannelParam.getPlatformChannelCode());
        return appPlatformChannelMapper.selectOne(qw);
    }
}
