package com.huiminpay.transaction.mq;

import com.huiminpay.transaction.api.dto.PayOrderMqDto;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Component
public class PayProducer {

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    /**
     * 延迟消息
     * @param topic
     * @param orderExt
     */
    public void sendMsgDelay(String topic, PayOrderMqDto payOrderMqDto){
        //发送同步消息，消息内容将orderExt转为json
        Message<PayOrderMqDto> message = MessageBuilder.withPayload(payOrderMqDto).build();
        //指定发送超时时间（毫秒）和延迟等级
        this.rocketMQTemplate.syncSend(topic,message,1000,3);
    }
}
