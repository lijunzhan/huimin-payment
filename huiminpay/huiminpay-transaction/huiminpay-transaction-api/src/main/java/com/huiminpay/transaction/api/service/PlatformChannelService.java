package com.huiminpay.transaction.api.service;

import com.huiminpay.common.cache.domain.BusinessException;
import com.huiminpay.transaction.api.dto.PayChannelDTO;
import com.huiminpay.transaction.api.dto.PayChannelParamDTO;
import com.huiminpay.transaction.api.dto.PlatformChannelDTO;

import java.util.List;

public interface PlatformChannelService {
    /**
     * 查询平台下所有服务
     * @return
     */
    List<PlatformChannelDTO> findAllPlatformChannes();

    /**
     * 商户应用绑定服务/（开启服务）
     * @param appId
     * @param platformChannel
     * @return
     * @throws BusinessException
     */
    Object openService(String appId, String platformChannel) throws BusinessException;

    /**
     * 查询服务是否被绑定
     * @param appId
     * @param platformChannel
     * @return
     */
    int queryAppBindPlatformChannel(String appId, String platformChannel);

    /**
     * 查新服务类型下的支付渠道
     * @param platformChannelCode
     * @return
     */
    List<PayChannelDTO> queryPlatformChannels(String platformChannelCode);

    /**
     * 保存支付渠道参数
     * @param payChannelParam（DTO支付渠道参数）
     * @throws BusinessException
     */
    void savePayChannelParam(PayChannelParamDTO payChannelParam) throws BusinessException;

    /**
     * 查询单个应用下的单个平台服务下的支付通道参数
     * @param appId
     * @param platformChannel
     * @param payChannel
     * @return
     */

    PayChannelParamDTO queryPayChannelParam(String appId, String platformChannel, String payChannel);

    /**
     * 获取指定应用指定服务类型下所包含的原始支付渠道参数列表
     * @param appId
     * @param platformChannel
     * @return
     */
    List<PayChannelParamDTO> queryPayChannelParams(String appId, String platformChannel);
}
