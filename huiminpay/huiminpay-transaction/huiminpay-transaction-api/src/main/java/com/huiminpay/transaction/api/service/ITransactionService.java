package com.huiminpay.transaction.api.service;

import com.huiminpay.transaction.api.dto.OrderResultDTO;
import com.huiminpay.transaction.api.dto.PayOrderDTO;
import com.huiminpay.transaction.api.dto.QrcodeDTO;

public interface ITransactionService {
    /**
     * 生成二维码入口url地址
     * @param qrcodeDTO
     * @return
     */
    String createQrUrl(QrcodeDTO qrcodeDTO);

    String createAliPayOrder(PayOrderDTO payOrderDTO) throws Exception;

    void updatePayOrderByTradeNo(OrderResultDTO orderResultDTO);

    OrderResultDTO selectPayOrder(String outTradeNo);
}
