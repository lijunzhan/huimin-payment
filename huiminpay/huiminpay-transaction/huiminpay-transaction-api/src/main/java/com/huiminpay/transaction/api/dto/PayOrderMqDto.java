package com.huiminpay.transaction.api.dto;

import lombok.Data;

import java.io.Serializable;
@Data
public class PayOrderMqDto implements Serializable {
    private String outTradeNo;
    private String payChannel;
}
