package com.huiminpay.transaction.api.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class QrcodeDTO implements Serializable {
    private String appId; //应用id
    private Long merchantId; //商户id
    private Long storeId; //门店id
    private String subject;//商品标题(店铺名)
    private String body;//订单描述（备注）
    private String qrUrl; //url地址


}
